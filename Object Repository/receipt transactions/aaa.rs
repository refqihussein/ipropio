<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>aaa</name>
   <tag></tag>
   <elementGuidId>297a1337-a119-464c-a17c-1f2544230404</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__layout']/div/div/main/div/div/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Receipts Transactions
                 Export
                                PDF
                             
                                XLS
                             Filters: Date Range       4 next 7 daysnext 30 daysprevious 7 daysprevious 30 days « ‹ » › Aug 2019 2010 ~ 2019 2019-08-05 SunMonTueWedThuFriSat28293031123456789101112131415161718192021222324252627282930311234567 2010201120122013201420152016201720182019 JanFebMarAprMayJunJulAugSepOctNovDec 000102030405060708091011121314151617181920212223000102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950515253545556575859000102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950515253545556575859 « ‹ » › Sep 2019 2010 ~ 2019 2019-09-04 SunMonTueWedThuFriSat2526272829303112345678910111213141516171819202122232425262728293012345 2010201120122013201420152016201720182019 JanFebMarAprMayJunJulAugSepOctNovDec 000102030405060708091011121314151617181920212223000102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950515253545556575859000102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950515253545556575859  Receipt Number  Receipts Status 
            Cancelled
               Loading...  Payment Type 
            All
               Loading...  #Receipt No.Payment TypeTransaction DateReceipt StatusPaid AmountNo matching records&lt;&lt;&lt;>>>0 records</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__layout&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;app-body&quot;]/main[@class=&quot;main&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;animated fadeIn receipt-list&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__layout']/div/div/main/div/div/div/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please click the receipts number in the tables to see receipts details'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Now'])[2]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div/div/div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
